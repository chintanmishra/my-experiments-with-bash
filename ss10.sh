#Escape sequences
echo "Hey World, \nWhat's up?"
echo "Hey World \rWhat's up?"
echo "Hey World \tWhat's up?"
echo "Hey world, \b\b\b\b\b\b\bwhat's up?"
echo "\033[7mHey World, What's up?\033[0m"
