echo "input a username: \c"
read logname
line=`grep $logname /etc/passwd`
IFS=:
set $line
echo "Username: $1"
echo "Password: $2"
echo "User ID : $3"
echo "Group ID: $4"
echo "Comment : $5"
echo "Home    : $6"
echo "Shell   : $7"
