echo "Enter a filename: \c"
read fname

terminal=`tty`

exec < $fname

nol=0
now=0

while read line
do
	nol=`expr $nol + 1`
	set $line
	now=`expr $now + $#`
done

echo "Number of lines = $nol"
echo "Number of words = $now"

# Alternate but O(n^2) method
while read line
do
	nol=`expr $nol + 1`
	exec < line
	while read words
	do
		now=`expr $now + 1`
	done
done

echo "$nol, $now"

exec < $terminal
