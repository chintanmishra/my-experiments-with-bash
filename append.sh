echo "Enter file name: \c"
read fname

if [ -f $fname ]
then
	if [ -w $fname ]
	then
		echo "Type content to append. To quit press Ctrl + D"
		cat >> $fname
	else
		echo "Not enough permission"
	fi
fi
