echo "Enter a word: \c"
read var

case $var in
	[aeiou]* | [AEIOU]*)
		echo "The word begins with a vowel"
		;;
	[0-9]*)
		echo "The word begins with a digit"
		;;
	*[0-9])
		echo "The word ende with digits"
		;;
	???)
		echo "You entered a three letter word"
		;;
	*)
		echo "Cannot understand the input"
		;;
esac
