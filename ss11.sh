#tput in action
tput clear
echo "Total number of rows in terminal window=\c"
tput lines
echo "Total number of columns in terminal window=\c"
tput cols
tput cup 15 20
tput bold
echo "This command should be bold"
echo "\033[0mAdios"
