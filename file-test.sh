echo "Enter a name: \c"
read fname

if [ -f $fname ]
then
	echo "You indeed entered a filename"
elif [ -d $fname ]
then
	echo "You picked a directory"
else
	echo "No file found"
fi

if [ -s $fname ]
then
	echo "size of file is greater than zero"
fi

# There are other options also available -r -w -x -c -s -b
