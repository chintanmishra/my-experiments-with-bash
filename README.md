# Bash practice

This repository is my experiments with GNU Bash. The purpose of this was to not feel complete stranger while reading a Bash script.

All the files here are just an extension of what was taught on this YouTube link
https://www.youtube.com/playlist?list=PL7B7FA4E693D8E790

The origial teacher provides this material for free on YouTube. So, these files are licensed under permissive BSD-license. For more details read LICENSE.md